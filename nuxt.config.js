export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  mode: 'spa',
  head: {
    title: 'newLogin',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
	  '~/plugins/firebase.js'
	],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    //'@nuxtjs/axios',
	//['@nuxtjs/firebase',
	//{
	//	config:{
	//		apiKey: "AIzaSyDr_jVmPzTzJKe-oZ2UBaR35OraEqOqj8Q",
	//		authDomain: "flutter-varios-3ce50.firebaseapp.com",
	//		databaseURL: "https://flutter-varios-3ce50-default-rtdb.firebaseio.com",
	//		projectId: "flutter-varios-3ce50",
	//		storageBucket: "flutter-varios-3ce50.appspot.com",
	//		messagingSenderId: "297471545901",
	//		appId: "1:297471545901:web:5a89652b290f96c82f1ea4"

	//	},
	//	services:{
	//		auth:{
	//			onAuthStateChangedMutation: 'ON_AUTH_STATE_CHANGED_MUTATION',
	//			onAuthStateChangedAction: 'onAuthStateChangedAction',
	//		}
	//	}
	//}
	//]
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: '/',
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
